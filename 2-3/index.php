<!DOCTYPE html>
<html>
<head>
    <title>JavaScript 2-3</title>
</head>
<body>

<p id = 'result'></p>

<form id = "calculate">
    <div>
        <input type = "text" name = "txt_one">
    </div>
    <div>
        <input type = "text" name = "txt_two">
    </div>
    
    <div>
        <label>Operator</label>
        <select type = "text" name = "operator">
            <option value = "*">*</option>
            <option value = "/">/</option>
            <option value = "+">+</option>
            <option value = "-">-</option>
        </select>
    </div>
    <div>
        <input type = "submit" value = "Submit">
    </div>
</form>

<script src = "main.js"></script>
</form>
</html>