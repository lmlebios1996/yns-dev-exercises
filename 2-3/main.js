const calculate = document.getElementById("calculate");
const result = document.getElementById("result");

calculate.onsubmit = function(event){
    event.preventDefault();

    let formData = new FormData(this);
    let inputOne = formData.get('txt_one');
    let inputTwo = formData.get('txt_two');
    let operation = formData.get('operator');

    switch(operation) {
        case '*':
            alert(inputOne + " * " + inputTwo + " = " + (parseInt(inputOne) * parseInt(inputTwo)));
            break;
        case '/':
            alert(inputOne + " / " + inputTwo + " = " + (parseInt(inputOne) / parseInt(inputTwo)));
            break;
        case '+':
            alert(inputOne + " + " + inputTwo + " = " + (parseInt(inputOne) + parseInt(inputTwo)));
            break;
        case '-':
            alert(inputOne + " - " + inputTwo + " = " + (parseInt(inputOne) - parseInt(inputTwo)));
            break;
        default:
    }

}