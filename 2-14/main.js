window.onload = function() {

    const pcAccessories = document.getElementById("pc_accessories");
    const image = document.getElementById("image");
    
    pcAccessories.onchange = function(){
        let accessory = this.value;
        image.src = "images/" + accessory;
    }

};