<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Calendar</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
        <link href="style.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-expand-lg sticky-top  navbar-dark bg-primary">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="../6-3/">Practice Systems / Programs</a>
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" href="../6-3/">Home</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="../6-1/">Quiz</a>
                </li>
                <li class="nav-item">
                <a class="nav-link active" aria-current="page">Calendar</a>
                </li>
            </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid p-5 text-black" id = "container">
        <h1>Calendar</h1>
        <button class = "btn btn-lg btn-primary" id = "forward"><span class = "fa fa-caret-up"></span></button>
        <button class = "btn btn-lg btn-primary" id = "previous"><span class = "fa fa-caret-down"></span></button>
        <h3 class = "float-end" id = "period">January-2021</h3>
      
        <div id = "calendar" class = "pt-3">
        </div>
    </div>
        <script src="jquery-3.6.0.min.js"></script>
        <script src="app.js"></script>
    </body>
</html>
