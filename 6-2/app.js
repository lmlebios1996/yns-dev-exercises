$(() => {
  
    const load_calendar = $("#calendar");
    const date = new Date();
    const period = $("#period");

    let today = date.getDate();
    let todayMonth = date.getMonth();
    let todayYear = date.getFullYear();

    let weekend = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thuresday', 'Friday', 'Saturday'];
    const previous = $("#previous");
    const forward = $("#forward");

    function loadCalendar(date){
        
        load_calendar.html("");

        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
        let firstDayWeekend = weekend[firstDay.getDay()];
        let day =1;

        
        let dateToday = date.getDate();
        let dateTodayMonth = date.getMonth();
        let dateTodayYear = date.getFullYear();

        weekend.forEach(function(item){
            load_calendar.append("<div class = 'day'>"+ item +"</div>");
        });

        // console.log(lastDay);

        for(let row=0; row < 35; row ++){
            if(weekend.indexOf(firstDayWeekend) <= row && lastDay >= day){

                let idToday = day == today && dateTodayYear == todayYear && dateTodayMonth == todayMonth ? "today" : "";
                load_calendar.append("<div class = 'day' id = '"+ idToday +"'>"+ day +"</div>");
                day++;
            } else {
                load_calendar.append("<div class = 'day'></div>");
            }
        }

        period.html(date.toLocaleString('default', { month: 'long' }) + "-" + dateTodayYear);

    }
    
    loadCalendar(date);

    $(document).on("click", "#previous", function(){
        date.setMonth(date.getMonth() - 1);
        loadCalendar(date);
    });

    $(document).on("click", "#forward", function(){
        date.setMonth(date.getMonth() + 1);
        loadCalendar(date);
    });


});

