<?php

$nameMessage = "";
$emailMessage = "";
$formMessage = "";
$name =  "";
$email =  "";
$validate = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $emailRegex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
    
    if(isset($_POST['name']) && !empty(textValidate($_POST['name'])))
    {
        $name = $_POST['name'] ;
        $validate = true;
    } 
        else 
    {
        $nameMessage = "Name is Required";
        $validate = false;
    }

    if(isset($_POST['email']) && !empty(textValidate($_POST['email'])))
    {
        $email = $_POST['email'] ;
        $validate = true;
    } 
        else 
    {
        $emailMessage = "Email is Required";
        $validate = false;
    }

    if(!preg_match($emailRegex, $email)){
        $emailMessage = "Invalid Email format"; 
        $validate = false;
    }

    if($validate){

        $file = fopen("users.csv","a");
        fputcsv($file, array($name, $email));
        fclose($file);

        $name =  "";
        $email =  "";

        $formMessage = "Successfully Saved.";

    }

}   

function textValidate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}



?>