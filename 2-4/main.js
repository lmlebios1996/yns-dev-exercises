window.onload = function() {

    const calculate = document.getElementById("calculate");
    const result = document.getElementById("result");

    calculate.onsubmit = function(event){
        event.preventDefault();

        let formData = new FormData(this);
        let number = formData.get('number');
        let primeNumbers = "Result: ";

        for(let i = 2; i <= number; i++)
        {
            let isPrime = true;
            for (let x = 2; x < i; x++) {
                if (i % x == 0) {
                    isPrime  = false;
                    break;
                } 
            }

            if(isPrime){
                primeNumbers += (i + ", ");
            }
        }

        result.innerHTML = primeNumbers.slice(0, -2);

    }
   
};