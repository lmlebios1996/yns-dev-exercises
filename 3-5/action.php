<?php

$nameMessage = "";
$passwordMessage = "";
$emailMessage = "";
$formMessage = "";
$imageMessage = "";
$loginMessage = "";
$contactNoMessage = "";
$name =  "";
$email =  "";
$contactNo = "";
$validate = false;
$imageDir = "Images/";
$userID = "";
$password = "";

session_start();

function session(){
    if(isset($_SESSION['user_ID']) && basename($_SERVER['PHP_SELF']) == 'index.php'){
        header('location:users.php');
    } elseif (!isset($_SESSION['user_ID']) && basename($_SERVER['PHP_SELF']) != 'index.php') {
        header('location:index.php');
    }
}

function dbConnect(){

    $host = "localhost";
    $username = "root";
    $password = "";
    $db = "yns_dev_exercises";

    return mysqli_connect($host, $username, $password, $db);

}

session();

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['save_user'])) {

    $emailRegex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
    $contactNoRegex = "/[^0-9]/";
    
    $file = $_FILES['file'];
    $fileName = $_FILES['file']['name'];
    $fileTempName = $_FILES['file']['tmp_name'];
    $fileType = $_FILES['file']['type'];

    $fileExt = explode('.', strtolower($fileName));
    $imageExt = array('jpg', 'jpeg', 'png');
    
    if(isset($_POST['name']) && !empty(textValidate($_POST['name'])))
    {
        $name = $_POST['name'] ;
        $validate = true;
    } 
        else 
    {
        $nameMessage = "Name is Required";
        $validate = false;
    }

    if(isset($_POST['email']) && !empty(textValidate($_POST['email'])))
    {
        $email = $_POST['email'] ;
        $validate = true;
    } 
        else 
    {
        $emailMessage = "Email is Required.";
        $validate = false;
    }

    if(isset($_POST['contact_no']) && !empty(textValidate($_POST['contact_no'])))
    {
        $contactNo = $_POST['contact_no'] ;
        $validate = true;
    } 
        else 
    {
        $contactNoMessage = "Contact Number is Required.";
        $validate = false;
    }

    //
    if(isset($_POST['password']) && !empty(textValidate($_POST['password'])))
    {
        $password = $_POST['password'] ;
        $validate = true;
    } 
        else 
    {
        $passwordMessage = "Password is Required.";
        $validate = false;
    }

    //

    if(!preg_match($emailRegex, $email)){
        $emailMessage = "Invalid Email format."; 
        $validate = false;
    }

    if(preg_match($contactNoRegex, $contactNo)){
        $contactNoMessage = "Invalid Contact Number."; 
        $validate = false;
    }

    if(!in_array(end($fileExt), $imageExt)){
        $imageMessage = "Please select an image."; 
        $validate = false;
    }

    $fileRename = uniqid('', true).".".end($fileExt);

    if(!move_uploaded_file($fileTempName, ($imageDir . $fileRename))){
        $imageMessage = "Unable to move the image."; 
        $validate = false;
    }

    if($validate){

        $conn = dbConnect();
    
        $sql = "INSERT INTO users(image, username, password, email, contact_number) 
                VALUES ('".$fileRename."', '".$name."', '".MD5($password)."', '".$email."', '".$contactNo."')";
        $query = mysqli_query($conn, $sql);

        $name =  "";
        $email =  "";
        $contactNo =  "";
        $password = "";

        $formMessage = "Successfully Saved.";

    }

}   

function textValidate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function usersList(){   

    $conn = dbConnect();
    
    $offset = 0;
    $rowCount = 10;

    if(isset($_GET['page']) && $_GET['page'] > 1){
        $offset = $_GET['page'];
    }
    
    $sql = "select image, username, email, contact_number from users limit " . $offset .", ". $rowCount;
    $query = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($query);

    if($count > 0){
       
        while( $row = mysqli_fetch_array($query)){
            echo "<tr>";       
            echo "<td align = 'center'> <img src = 'Images/".$row['image'] . "' width='20px' height='20px'></td>";
            echo "  <td>".$row['username']."</td>";
            echo "  <td>".$row['email']."</td>";
            echo "  <td>".$row['contact_number']."</td>";
            echo "<tr>";
        }

        
        $sql = "select image, username, email from users ";
        $query = mysqli_query($conn, $sql);
        $count = mysqli_num_rows($query);

        $totalPages = ceil($count / $rowCount);    

        echo '<tr>';
            echo '<td colspan = "3" align = "center">';
            for($page = 1; $page<= $totalPages; $page++) {  
                echo "<a class = 'pagination' href = 'users.php?page=" . ($page > 1 ? (($page -1) * 10) : $page) . "'>" . $page . " </a>";  
            }
            echo '</td>';
        echo '</tr>';

    } else {
        echo "<tr>";
        echo "  <td colspan = '3' align = 'center'>No Available Data</td>";
        echo "<tr>";

    }
    
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['login'])) {

    //username Milo
    //Password password
    
    $conn = dbConnect();
    $userID = mysqli_real_escape_string($conn, $_POST['user_ID']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);

    $sql = "select id from users where username = '".$userID."' && password = '".MD5($password)."'";
    $query = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($query);

    if($count > 0){
        $row = mysqli_fetch_array($query);
        $_SESSION['user_ID'] = $row['id'];
        header('location:index.php');
    } else $loginMessage = "Invalid credentials.";


}



?>