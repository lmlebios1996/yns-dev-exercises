<!DOCTYPE html>
<html>
<head>
    <title>User Information</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
<?php
    require 'action.php';
?>

<a href = "add_user.php">New User</a>
<a href = "logout.php" style = "margin-left: 50%">Logout</a>

   <table>
       <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
        </thead>
        <tbody>
            <?php usersList(); ?>
        </tbody>
    </table>

</html>