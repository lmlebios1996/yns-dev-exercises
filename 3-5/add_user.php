<!DOCTYPE html>
<html>
<head>
    <title>User Information</title>
</head>
<body>
<?php
    require 'action.php';
    echo $formMessage;
?>
    <form action = "" method = "post"  enctype="multipart/form-data">
        <div>
            <label>Image</label><br>
            <input type = "file" name = "file"> <?php echo $imageMessage ?>
        </div>
        <div>
            <label>User Name</label><br>
            <input type = "text" name = "name" value = "<?php echo $name ?>">  <?php echo $nameMessage ?>
        </div>

        <div>
            <label>Password</label><br>
            <input type = "password" name = "password" value = "<?php echo $password ?>">  <?php echo $passwordMessage ?>
        </div>
        
        <div>
            <label>Contact Number</label><br>
            <input type = "text" name = "contact_no" value = "<?php echo $contactNo ?>">  <?php echo $contactNoMessage ?>
        </div>
        <div>
            <label>E-mail</label><br>
            <input type = "text" name = "email" value = "<?php echo $email ?>">  <?php echo $emailMessage ?>
        </div>
        <div>
            <input type = "submit" name = "save_user" value = "Submit">
            <a href = "users.php">Cancel</a>
        </div>
    </form>
</body>

</html>