window.onload = function() {

    const container = document.getElementById("container");
    const toBottom = document.getElementById("to_bottom");
    const toTop = document.getElementById("to_top");

    let height = screen.height;
    container.style.height = (height * 2)+"px";

    toTop.style.marginTop =  ((height * 2) - 42)+"px";

    toBottom.onclick = function(){
        window.scrollBy(0, (height * 2));
    }

    toTop.onclick = function(){
        window.scrollBy(0, -(height * 2));
    }

};