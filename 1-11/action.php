<?php

$nameMessage = "";
$emailMessage = "";
$formMessage = "";
$imageMessage = "";
$name =  "";
$email =  "";
$validate = false;
$imageDir = "Images/";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $emailRegex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
    
    $file = $_FILES['file'];
    $fileName = $_FILES['file']['name'];
    $fileTempName = $_FILES['file']['tmp_name'];
    $fileType = $_FILES['file']['type'];

    $fileExt = explode('.', strtolower($fileName));
    $imageExt = array('jpg', 'jpeg', 'png');
    
    if(isset($_POST['name']) && !empty(textValidate($_POST['name'])))
    {
        $name = $_POST['name'] ;
        $validate = true;
    } 
        else 
    {
        $nameMessage = "Name is Required";
        $validate = false;
    }

    if(isset($_POST['email']) && !empty(textValidate($_POST['email'])))
    {
        $email = $_POST['email'] ;
        $validate = true;
    } 
        else 
    {
        $emailMessage = "Email is Required.";
        $validate = false;
    }

    if(!preg_match($emailRegex, $email)){
        $emailMessage = "Invalid Email format."; 
        $validate = false;
    }

    if(!in_array(end($fileExt), $imageExt)){
        $imageMessage = "Please select an image."; 
        $validate = false;
    }

    $fileRename = uniqid('', true).".".end($fileExt);

    if(!move_uploaded_file($fileTempName, ($imageDir . $fileRename))){
        $imageMessage = "Unable to move the image."; 
        $validate = false;
    }

    if($validate){

        $file = fopen("users.csv","a");
        fputcsv($file, array($fileRename, $name, $email));
        fclose($file);

        $name =  "";
        $email =  "";

        $formMessage = "Successfully Saved.";

    }

}   

function textValidate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function usersList(){   

    $rows = array_map('str_getcsv', file('users.csv'));
    $header = array_shift($rows);
    $users = array();
    foreach ($rows as $row) {
        $users[] = array_combine($header, $row);
    }

    foreach ($users as $user){
        echo '<tr>';
            echo "<td align = 'center'> <img src = 'Images/".$user['Image'] . "' width='50%' height='85'></td>";
            echo "<td>".$user['Name'] . "</td>";
            echo "<td>".$user['Email'] . "</td>";
        echo '</tr>';
    }

}

?>