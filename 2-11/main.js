window.onload = function() {

    const body = document.body;
    
    let red = 0;
    let green = 0;
    let blue = 139;
    
    //rgb(0,0,139) to rgb(173,216,230)

    let frame = function () {
        if(red != 173){
            red += 1;
        } else if (green != 216){
            green += 1;
        } else if (blue != 230){
            blue += 1;
        } else {
            clearInterval(id);
        }

        body.style.backgroundColor = "rgb("+ red +"," + green + "," + blue + ")";
    }

    let id = setInterval(frame, 10);
};