window.onload = function() {

    const addLabel = document.getElementById("add_label");
    const container = document.getElementById("container");

    addLabel.onclick = function(){

        const nodeLabel = document.createElement("label");
        const labelNode = document.createTextNode("New label");
        nodeLabel.appendChild(labelNode);
        container.appendChild(nodeLabel);

    }
   
};