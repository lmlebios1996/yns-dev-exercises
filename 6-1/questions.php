<?php

require 'conn.php';
$questionsSql = "select * from questions  order by rand()";
$questionsQuery = mysqli_query($conn, $questionsSql);
$data = array();

while($questionsRow = mysqli_fetch_array($questionsQuery)){
    
    $questions = array();
    $choices = array();

    $questions[] = $questionsRow;
     
    $choicesSql = "select * from choices where question_id = {$questionsRow['id']}  order by rand()";
    $choicesQuery = mysqli_query($conn, $choicesSql);
    while($choicesRow = mysqli_fetch_array($choicesQuery)){
        $choices["choices"][] = $choicesRow;
    }

    $data[] = array_merge($questions, $choices);

}

echo json_encode ($data);

?>