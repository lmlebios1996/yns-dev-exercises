$(() => {
    
    let questions;
    let answer = [];
    let questionStart = 0;
    let totalQuestions = 0;
    const container = $("#container");

    $(document).on('click', '#start', function(){
        $.ajax({
            url: "questions.php",

            success: (data) => {
                questions = JSON.parse(data);
                totalQuestions = questions.length;
                loadQuestion(questionStart);
            }

        });
    });


    let loadQuestion = (indexFrom) => {
        let indexTo = indexFrom + 1;
        
        questions.slice(indexFrom, indexTo).forEach((question) => {
            let render = "<h1>PHP Quiz</h1>";
            render += "<p>Question "+ indexTo +" of "+ totalQuestions +"</p> ";
            render += "<p>"+ question[0].question +"</p> ";

            render += "<form id = 'save'>";
            (question.choices).forEach((choices) => {
                render += "<ul class='list-group'> ";
                render += "<li class = 'list-group-item'><input type = 'radio' name = '"+ question[0].id +"' value = '"+ choices.id +"'> <code>"+ choices.description +"</code></li> ";
                render += "</ul> ";
            });

            render += "<button class = 'btn btn-primary mt-2' type = 'submit'>Next</button>";
            render += "</form> ";
            container.html(render);
        });
    }

    $(document).on('submit', '#save', function(event){
        event.preventDefault();
        let formValue = $(this).serializeArray();
        answer.push(formValue[0].value);
        questionStart++;

        if(questionStart < totalQuestions){
            loadQuestion(questionStart);
        } else {
            loadResult();
        }
    });

    let loadResult = () => {
        
        let correctAnswerCount = 0;
        let render = "<h1>PHP Quiz Result</h1>";
        render += "<p id = 'score'>Score 4 of 4</p>";
        render += "<p id = 'score_percent'>100% Correct</p>";
            questions.forEach((question) => {
                render += "<div class = 'pt-3'>";
                render += "<p>"+ question[0].question +"</p> ";

                var answerId = (question.choices).find((item) => {
                    return item.correct_answer == 1;
                });
                correctAnswerCount += answer.includes(answerId.id) == 1 ? 1 : 0;
                let border = answer.includes(answerId.id) == 1 ? "border-success" : "border-danger";

                (question.choices).forEach((choices) => {
                    let status = choices.correct_answer == 1 ? "checked" : "";
                    let myAnswer = answer.includes(choices.id) ? "<span class = 'badge bg-secondary float-end'>Your Answer</span>" : "";
                    render += "<ul class='list-group'> ";
                    render += "<li class = 'list-group-item border "+ border +"'><input type = 'checkbox' "+ status +"> <code>"+ choices.description +"</code> "+ myAnswer +"</li> ";
                    render += "</ul> ";
                });
                render += "</div>";
            });
        
        container.html(render);
        $("#score").html("Score "+ correctAnswerCount + " of "+ totalQuestions +"");
        $("#score_percent").html(((correctAnswerCount / totalQuestions) * 100 ) + "% Correct");
    }

});

