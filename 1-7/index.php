<!DOCTYPE html>
<html>
<head>
    <title>HTML and PHP 1-7</title>
</head>
<body>

<?php

$nameMessage = "";
$emailMessage = "";
$name =  "";
$email =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $emailRegex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
    
    if(isset($_POST['name']) && !empty(textValidate($_POST['name'])))
    {
        $name = $_POST['name'] ;
    } else $nameMessage = "Name is Required";

    if(isset($_POST['email']) && !empty(textValidate($_POST['email'])))
    {
        $email = $_POST['email'] ;
    } else $emailMessage = "Email is Required";

    if(!preg_match($emailRegex, $email)){
        $emailMessage = "Invalid Email format";
    }

}   

function textValidate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>

    <form action = "" method = "post">
        <div>
            <label>Name</label>
            <input type = "text" name = "name" value = "<?php echo $name ?>">  <?php echo $nameMessage ?>
        </div>
        <div>
            <label>E-mail</label>
            <input type = "text" name = "email" value = "<?php echo $email ?>">  <?php echo $emailMessage ?>
        </div>
        <div>
            <input type = "submit" name = "operator" value = "Submit">
        </div>
    </form>
</body>

</html>