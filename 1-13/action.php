<?php

$nameMessage = "";
$emailMessage = "";
$formMessage = "";
$imageMessage = "";
$loginMessage = "";
$name =  "";
$email =  "";
$validate = false;
$imageDir = "Images/";
$userID = "";

session_start();

function session(){
    if(isset($_SESSION['user_ID']) && basename($_SERVER['PHP_SELF']) == 'index.php'){
        header('location:users.php');
    } elseif (!isset($_SESSION['user_ID']) && basename($_SERVER['PHP_SELF']) != 'index.php') {
        header('location:index.php');
    }
}

session();

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['save_user'])) {

    $emailRegex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
    
    $file = $_FILES['file'];
    $fileName = $_FILES['file']['name'];
    $fileTempName = $_FILES['file']['tmp_name'];
    $fileType = $_FILES['file']['type'];

    $fileExt = explode('.', strtolower($fileName));
    $imageExt = array('jpg', 'jpeg', 'png');
    
    if(isset($_POST['name']) && !empty(textValidate($_POST['name'])))
    {
        $name = $_POST['name'] ;
        $validate = true;
    } 
        else 
    {
        $nameMessage = "Name is Required";
        $validate = false;
    }

    if(isset($_POST['email']) && !empty(textValidate($_POST['email'])))
    {
        $email = $_POST['email'] ;
        $validate = true;
    } 
        else 
    {
        $emailMessage = "Email is Required.";
        $validate = false;
    }

    if(!preg_match($emailRegex, $email)){
        $emailMessage = "Invalid Email format."; 
        $validate = false;
    }

    if(!in_array(end($fileExt), $imageExt)){
        $imageMessage = "Please select an image."; 
        $validate = false;
    }

    $fileRename = uniqid('', true).".".end($fileExt);

    if(!move_uploaded_file($fileTempName, ($imageDir . $fileRename))){
        $imageMessage = "Unable to move the image."; 
        $validate = false;
    }

    if($validate){

        $file = fopen("users.csv","a");
        fputcsv($file, array($fileRename, $name, $email));
        fclose($file);

        $name =  "";
        $email =  "";

        $formMessage = "Successfully Saved.";

    }

}   

function textValidate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function usersList(){   

    $userListStart = 0;
    $userListLength = 10;

    if(isset($_GET['page']) && $_GET['page'] > 1){
        $userListStart = $_GET['page'];
    }

    $rows = array_map('str_getcsv', file('users.csv'));
    $header = array_shift($rows);
    $users = array();
    foreach ($rows as $row) {
        $users[] = array_combine($header, $row);
    }

    $OverAllCount = count($users);
    $users = array_slice($users, $userListStart, $userListLength);
    $PerPageCount = count($users);

    foreach ($users as $user){
        echo '<tr>';
            echo "<td align = 'center'> <img src = 'Images/".$user['Image'] . "' width='20px' height='20px'></td>";
            echo "<td>".$user['Name'] . "</td>";
            echo "<td>".$user['Email'] . "</td>";
        echo '</tr>';
    }

    $totalPages = ceil($OverAllCount / $userListLength);    

    echo '<tr>';
        echo '<td colspan = "3" align = "center">';
    for($page = 1; $page<= $totalPages; $page++) {  
        echo "<a class = 'pagination' href = 'index.php?page=" . ($page > 1 ? (($page -1) * 10) : $page) . "'>" . $page . " </a>";  
    }
        echo '</td>';
    echo '</tr>';
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['login'])) {

    //username Milo
    //Password password
    
    $userID = $_POST['user_ID'];

    $rows = array_map('str_getcsv', file('users.csv'));
    $header = array_shift($rows);
    $users = array();
    foreach ($rows as $row) {
        $users[] = array_combine($header, $row);
    }

    $user = count(array_filter($users, function($value) {
        return $value['Name'] == $_POST['user_ID'] && $value['Password'] == md5($_POST['password']);
    } ));

    if($user > 0){
            $_SESSION['user_ID'] = $_POST['user_ID'];
            header('location:index.php');
    } else $loginMessage = "Invalid credentials.";

}



?>