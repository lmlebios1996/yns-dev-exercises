<!DOCTYPE html>
<html>
<head>
    <title>User Information</title>
</head>
<body>
<?php
    require 'action.php';
    echo $formMessage;
?>
    <form action = "" method = "post"  enctype="multipart/form-data">
        <div>
            <label>Image</label>
            <input type = "file" name = "file"> <?php echo $imageMessage ?>
        </div>
        <div>
            <label>Name</label>
            <input type = "text" name = "name" value = "<?php echo $name ?>">  <?php echo $nameMessage ?>
        </div>
        <div>
            <label>E-mail</label>
            <input type = "text" name = "email" value = "<?php echo $email ?>">  <?php echo $emailMessage ?>
        </div>
        <div>
            <input type = "submit" name = "save_user" value = "Submit">
            <a href = "users.php">Cancel</a>
        </div>
    </form>
</body>

</html>