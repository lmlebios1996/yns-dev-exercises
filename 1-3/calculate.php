<?php


function GreatestCommonDivisor($largest, $smallest){
    
    if(($largest % $smallest)  <> 0){
        GreatestCommonDivisor($smallest, ($largest % $smallest));
    } else {
        echo $smallest;
    }
  
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $txt_one = 0 ;
    $txt_two = 0 ;
    $validator = false;

    if(isset($_POST['txt_one']))
    {
        $txt_one = $_POST['txt_one'] ;
        $validator = true;
    } else $validator = false;
    
    if(isset($_POST['txt_two']))
    {
        $txt_two = $_POST['txt_two'] ;
        $validator = true;
    } else $validator = false;

    if($validator){
        if($txt_one >= $txt_two){
            GreatestCommonDivisor($txt_one, $txt_two);
        } else {
            GreatestCommonDivisor($txt_two, $txt_one);
        }
    }
}   


?>