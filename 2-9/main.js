window.onload = function() {

    const changeColor = document.getElementsByClassName("change_color");
    const countColors = changeColor.length;
    const body = document.body;

    for(let i=0; i < countColors; i++){
        changeColor[i].onclick = function (){
            body.style.backgroundColor = this.value
        }
    }

};