/* Create tables and insert data */
/* 1) Create all the tables in your MySQL. Try to define primary key, data type, not null constraint. */

    CREATE TABLE departments (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(250) NOT NULL,
        PRIMARY KEY(id)
    );

    CREATE TABLE positions (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(250) NOT NULL,
        PRIMARY KEY(id)
    );

    CREATE TABLE employees (
        id INT NOT NULL AUTO_INCREMENT,
        first_name VARCHAR(250) NOT NULL,
        last_name VARCHAR(250),
        middle_name VARCHAR(250),
        birth_date DATE NOT NULL,
        department_id INT,
        hire_date DATE,
        boss_id INT,
        PRIMARY KEY(id),
        constraint fk_type foreign key(department_id) references departments(id)
    );

    CREATE TABLE employee_positions (
        id INT NOT NULL AUTO_INCREMENT,
        employee_id INT NOT NULL,
        position_id INT NOT NULL,
        PRIMARY KEY(id),
        constraint fk_employee foreign key(employee_id) references employees(id),
        constraint fk_position foreign key(position_id) references positions(id)
    );


/* 2) Insert all the data into the tables that you have created. */

    INSERT INTO `departments` (`id`, `name`) VALUES
    (1, 'Exective'),
    (2, 'Admin'),
    (3, 'Sales'),
    (4, 'Development'),
    (5, 'Design'),
    (6, 'Marketing');

    INSERT INTO `positions` (`id`, `name`) VALUES
    (1, 'CEO'),
    (2, 'CTO'),
    (3, 'CFO'),
    (4, 'Manager'),
    (5, 'Staff');

    INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
    (1, 'Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL),
    (2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
    (3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1),
    (4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
    (5, 'Lorraine', 'San Jose', 'P', '1983-10-11', 2, '2015-03-10', 1),
    (6, 'Haille', 'Dela Cruz', 'A', '1990-11-12', 3, '2015-02-15', 1),
    (7, 'Godfrey', 'Sarmenta', 'L', '1993-09-13', 4, '2015-01-01', 1),
    (8, 'Alex', 'Amistad', 'F', '1988-04-14', 4, '2015-04-10', 1),
    (9, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1),
    (10, 'Kim', NULL, NULL, '1977-10-16', 5, '2015-08-06', 1);


    INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES
    (1, 1, 1),
    (2, 1, 2),
    (3, 1, 3),
    (4, 2, 4),
    (5, 3, 5),
    (6, 4, 5),
    (7, 5, 5),
    (8, 6, 5),
    (9, 7, 5),
    (10, 8, 5),
    (11, 9, 5),
    (12, 10, 5);


/* * Exercise SELECT statement */
/* 1) Retrieve employees whose last name start with "K". */
    select
        id,
        first_name,
        last_name,
        middle_name,
        department_id,
        hire_date,
        boss_id
    from
        employees
    where
        substr(upper(last_name), 1, 1) = 'K';

/* 2) Retrieve employees whose last name end with "i". */

    select
        id,
        first_name,
        last_name,
        middle_name,
        department_id,
        hire_date,
        boss_id
    from
        employees
    where
        right(upper(last_name), 1) = 'I';

/* 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date. (Hint. Use CONCAT)*/
    
    select
        CONCAT(first_name, " ", middle_name, ". ", last_name) full_name,
        hire_date
    from
        employees
    where
        hire_date between '2015-1-1'
        and '2015-3-31'
    order by
        hire_date

/* 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.*/

    select
        emp.last_name Employee,
        boss.last_name Boss
    from
        employees emp
        inner join employees boss on emp.boss_id = boss.id;

/* 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.*/


    select
        emp.last_name
    from
        employees emp
        left join departments dept on emp.department_id = dept.id
    WHERE
        dept.name = 'Sales'
    order by
        emp.last_name desc;

/* 6) Retrieve number of employee who has middle name.*/

    SELECT
        count(*) count_has_middle
    FROM
        employees
    WHERE
        middle_name is not null

/* 7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.*/

    SELECT
        dept.name,
        count(D.id)
    FROM
        departments dept
        INNER join employees d on dept.id = d.department_id
    group by
        dept.name
    order by
        dept.id


/* 8) Retrieve employee's full name and hire date who was hired the most recently.*/

    select
        first_name,
        middle_name,
        last_name,
        hire_date
    from
        employees
    order by
        hire_date
    limit
        1

/* 9) Retrieve department name which has no employee.*/

    SELECT
        upper(dept.name) name,
        count(D.id) id
    FROM
        departments dept
        left join employees d on dept.id = d.department_id
    group by
        dept.name
    HAVING
        count(D.id) = 0

/* "10) Retrieve employee's full name who has more than 2 positions */

    select
        emp.first_name,
        emp.middle_name,
        emp.last_name
    from
        employees emp
        left join employee_positions empPoss on emp.id = empPoss.employee_id
    group by
        emp.first_name,
        emp.middle_name,
        emp.last_name
    having
        count(empPoss.position_id) > 1

/* "*/