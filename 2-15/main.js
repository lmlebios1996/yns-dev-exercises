window.onload = function() {

    const datetime = document.getElementById("datetime");

    setInterval(dateTime, 60000);

    function dateTime() {
        const date = new Date();
        document.getElementById("datetime").innerHTML =  date.toDateString() + " " + formatAMPM(date);
  
    }

    dateTime();

    function formatAMPM(date) {
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; 
        minutes = minutes < 10 ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
      }
};