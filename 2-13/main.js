window.onload = function() {

    const pcAccessories = document.getElementsByClassName("imgage_sizes");
    const countPcAccessories = pcAccessories.length;
    const image = document.getElementById("image"); 
    
    for(let i=0; i < countPcAccessories; i++){
        pcAccessories[i].onclick = function (){
            image.style.width = this.value;
            image.style.height = 'auto';
        }
    }

};